import tensorflow as tf


def print_tf_version():
    print("--------------------------------------------------------------------------------------")
    print(tf.__version__)
    # each line should not longer than 99, you can try making following line over 99 and
    # test with linter: flake8
    print("--------------------------------------------------------------------------------------")
