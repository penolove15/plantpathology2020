import os
import pickle

import pandas as pd


class EnsembleCandidate():
    def __init__(self, submission_dir):
        self.submission_dir = submission_dir
        self.submission_path = os.path.join(submission_dir, "submission.csv")
        self.validation_pred_path = os.path.join(submission_dir, "validation_out.pkl")

    @property
    def validation_pred(self):
        """
        Returns
        -------
        np.ndarray
            the prediction scores of validation set which shape should be (n, 4)
        """
        with open(self.validation_pred_path, "rb") as f:
            out = pickle.load(f)
        return out

    @property
    def testing_pred(self):
        """
        Returns
        -------
        np.ndarray
            the prediction scores of testing set which shape should be (n, 4)
        """
        sub = pd.read_csv(self.submission_path)
        return sub.loc[:, "healthy":].values
