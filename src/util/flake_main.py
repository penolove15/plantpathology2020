from util.flake_module import print_tf_version

if __name__ == "__main__":
    print_tf_version()
