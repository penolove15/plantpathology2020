import pickle


def get_folds_df(folds_path, target_folds, train, exclude=False):
    """
    Parameters
    ----------
    folds_path: str
        the folds pkl path

    target_folds: List[int]
        folds to be fetched

    train: DataFrame
        the original dataframe

    Returns
    -------
    DataFrame
        the selected folds df
    """
    # prepare the validation Y of the fold
    valid_folds = []
    with open(folds_path, "rb") as f:
        folds = pickle.load(f)
    for i in target_folds:
        valid_folds.append(folds == i)
    if exclude:
        valid_index = sum(valid_folds) == 0
    else:
        valid_index = sum(valid_folds) > 0
    return train[valid_index]
