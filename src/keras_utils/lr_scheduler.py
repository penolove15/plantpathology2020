import tensorflow as tf


def lrfn_in_notebook(epoch):
    """
    this lr fn were copied from
    https://www.kaggle.com/anyexiezouqu/tpu-incepresnetv2-enb7?scriptVersionId=31040965

    Parameters
    ----------
    epoch: int
        current epoch
    """
    LR_START = 1e-4
    LR_MAX = 4e-4
    LR_MIN = 1e-4
    LR_RAMPUP_EPOCHS = 4
    LR_SUSTAIN_EPOCHS = 6
    LR_EXP_DECAY = 0.8
    if epoch < LR_RAMPUP_EPOCHS:
        lr = (LR_MAX - LR_START) / LR_RAMPUP_EPOCHS * epoch + LR_START
    elif epoch < LR_RAMPUP_EPOCHS + LR_SUSTAIN_EPOCHS:
        lr = LR_MAX
    else:
        lr = (LR_MAX - LR_MIN) * LR_EXP_DECAY ** (
            epoch - LR_RAMPUP_EPOCHS - LR_SUSTAIN_EPOCHS
        ) + LR_MIN
    return lr


def get_lr_scheduler():
    lr_callback = tf.keras.callbacks.LearningRateScheduler(lrfn_in_notebook, verbose=True)
    return lr_callback
