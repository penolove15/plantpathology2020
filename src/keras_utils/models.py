import tensorflow as tf
import tensorflow.keras.layers as L
from tensorflow.keras.applications import InceptionResNetV2, DenseNet121, ResNet50V2
import efficientnet.tfkeras as efn

from util.config import IMAGE_SIZE, BACKBONE_TYPE


def backbone_factory(backbone_cfg):
    """
    parameters
    ----------
    backbone: str
        the expected backbone

    Returns
    -------
    keras.Layer
        the instance of keras layer,
        exceptd input tensor is a (W, H, 3) image,
        output tensor is (W', H', hidden_size)
    """
    image_size = backbone_cfg[IMAGE_SIZE]
    backbone_type = backbone_cfg[BACKBONE_TYPE]

    if backbone_type == "inception_resnet":
        backbone = InceptionResNetV2(
            input_shape=(*image_size, 3), weights="imagenet", include_top=False,
        )
    elif backbone_type == "resnet50_v2":
        backbone = ResNet50V2(
            input_shape=(*image_size, 3), weights="imagenet", include_top=False
        )
    elif backbone_type == "efficient_net_b3":
        backbone = efn.EfficientNetB3(
            input_shape=(*image_size, 3), weights="imagenet", include_top=False
        )
    elif backbone_type == "efficient_net_b4":
        backbone = efn.EfficientNetB4(
            input_shape=(*image_size, 3), weights="imagenet", include_top=False
        )
    elif backbone_type == "efficient_net_b7":
        backbone = efn.EfficientNetB7(
            input_shape=(*image_size, 3), weights="imagenet", include_top=False
        )
    elif backbone_type == "efficient_net_b7_noise":
        backbone = efn.EfficientNetB7(
            input_shape=(*image_size, 3), weights="noisy-student", include_top=False
        )
    elif backbone_type == "densenet121":
        backbone = DenseNet121(
            input_shape=(*image_size, 3), weights="imagenet", include_top=False
        )
    else:
        raise Exception("the backbone type not found")
    return backbone


def model_factory(backbone_cfg, n_label):
    """
    parameters
    ----------
    backbone_cfg: dict
        backbone config

    n_label:
        number of label

    Returns
    -------
    keras.Model
        exceptd input tensor is a (W, H, 3) image,
        output tensor is (n_label)
    """

    backbone = backbone_factory(backbone_cfg)
    model = tf.keras.Sequential(
        [backbone, L.GlobalAveragePooling2D(), L.Dense(n_label, activation="softmax")]
    )
    return model


def add_kernel_regularizer(model, regularizer):
    if not isinstance(regularizer, tf.keras.regularizers.Regularizer):
        print("Regularizer must be a subclass of tf.keras.regularizers.Regularizer")
        return model

    for layer in model.layers:
        for attr in ["kernel_regularizer"]:
            if hasattr(layer, attr):
                setattr(layer, attr, regularizer)
