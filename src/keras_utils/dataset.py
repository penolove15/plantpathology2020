import gc
from functools import partial

import tensorflow as tf
import numpy as np
from tensorflow.keras.callbacks import Callback


def decode_image(filename, label=None, image_size=(800, 800)):
    """
    read the image from file and resize the image

    Parameters
    ----------
    filename: str
        the image path to be read
    image_size: int
        resize the image
    label: np.ndarray
        the label array

    Returns
    -------
    Option[Tensor, (Tensor, np.ndarray)]
        output tensor(s)
    """
    bits = tf.io.read_file(filename)
    image = tf.image.decode_jpeg(bits, channels=3)
    image = tf.cast(image, tf.float32) / 255.0
    image = tf.image.resize(image, image_size)

    if label is None:
        return image
    else:
        return image, label


def data_augment(image, label=None):
    """
    do the augment with the image

    Parameters
    ----------
    image: tensor
        the image path to be read

    label: Option[np.ndarray]
        the label array

    Returns
    -------
    Option[Tensor, (Tensor, np.ndarray)]
        output tensor(s)
    """
    image = tf.image.random_flip_left_right(image)
    image = tf.image.random_flip_up_down(image)

    if label is None:
        return image
    else:
        return image, label


class PseudoLabelingDataset(Callback):
    def __init__(
        self,
        image_paths,
        label_array,
        decode_image_fn,
        batch_size,
        repeat_times,
        test_image_paths,
        image_size,
        pseudo_labeling_ratio=0.02,
        frequency=3,
    ):
        self.image_paths_ori = image_paths
        self.label_array_ori = label_array

        self.test_image_paths = test_image_paths

        self.decode_image_fn = decode_image_fn
        self.batch_size = batch_size
        self.image_size = image_size
        if repeat_times is not None:
            self.repeat_times = repeat_times
        else:
            self.repeat_times = 1000  # FIXME: just set a large number can't be reached

        # pseudo-labeling
        self.label_dist = np.sum(label_array, axis=0) / label_array.shape[0]
        self.pseudo_labeling_ratio = pseudo_labeling_ratio
        self.pseudo_labeled_paths = None
        self.pseudo_labeled_array = None
        self.frequency = frequency

    @property
    def image_paths(self):
        if self.pseudo_labeled_paths is None:
            return self.image_paths_ori
        else:
            return np.concatenate([self.image_paths_ori, self.pseudo_labeled_paths])

    @property
    def label_array(self):
        if self.pseudo_labeled_array is None:
            return self.label_array_ori
        else:
            return np.concatenate([self.label_array_ori, self.pseudo_labeled_array])

    def update_image_paths(self, epoch):
        """
        this function were used to implement the pseudo-labling to add additional
        information into dataset, it needs to be a callback like
        """
        if (epoch + 1) % self.frequency != 0:
            print("--------------------")
            print("skip tagging process")
        else:
            test_dataset = image_dataset_factory(
                self.test_image_paths,
                image_size=self.image_size,
                batch_size=self.batch_size,
            )

            prob = self.model.predict(test_dataset, verbose=1)

            label_counts = (
                self.label_dist
                * len(self.test_image_paths)
                * self.pseudo_labeling_ratio
                * epoch
            )

            pesudo_labeling_dict = {}
            unique_index = set()
            for idx, label_count in enumerate(label_counts):
                top_k = int(label_count)
                if top_k > 0:
                    index = np.argpartition(prob[:, idx], -top_k)[-top_k:]
                    pesudo_labeling_dict[idx] = set(index)
                    unique_index.update(set(index))

            selected_testing_index = sorted(unique_index)
            self.pseudo_labeled_paths = self.test_image_paths[selected_testing_index]
            self.pseudo_labeled_array = np.zeros(
                (len(self.pseudo_labeled_paths), len(label_counts))
            )
            labels = np.argmax(prob[selected_testing_index, :], axis=1)
            for idx, col in enumerate(labels):
                self.pseudo_labeled_array[idx, col] = 1
            print("-----------------------------------------------------")
            print("tagged %s images from testing set" % len(unique_index))
            print("tagged distribution: %s" % np.sum(self.pseudo_labeled_array, axis=0))

    def on_epoch_end(self, epoch, logs):
        self.update_image_paths(epoch)

    def generate(self):
        for i in range(self.repeat_times):
            print("-------------------------")
            print(
                "%s images in this epoch, %s"
                % (len(self.image_paths), self.label_array.shape)
            )
            dataset = (
                tf.data.Dataset.from_tensor_slices((self.image_paths, self.label_array))
                .map(self.decode_image_fn)
                .map(data_augment)
                .repeat(1)
                .shuffle(512)
                .batch(self.batch_size)
            )
            for j in dataset:
                yield j
            gc.collect()


def image_label_pesudo_labeling_dataset_factory(
    image_paths,
    label_array,
    batch_size=64,
    image_size=(800, 800),
    repeat_times=None,
    test_image_paths=None,
):
    """
    dataset factory, returns TF dataset, which contains two tensor
    1. Image tensor
    2. Label tensor

    Parameters
    ----------
    image_paths: List[str]
        image_paths with length N
    label_array: np.ndarray
        label array, expected shape, N, C
    batch_size: int
        batch size
    image_size: int
        expected generated image shape
    repeat_times: Option[int]
        repeat the dataset or not, if None -> indefinitely

    Returns
    -------
    PseudoLabelingDataset
        a object to wrapper a tf.data.Dataset object
        with the generate implementation that will returns generator can be fitted
    """
    decode_image_fn = partial(decode_image, image_size=image_size)

    dataset = PseudoLabelingDataset(
        image_paths,
        label_array,
        decode_image_fn,
        batch_size,
        image_size=image_size,
        repeat_times=repeat_times,
        test_image_paths=test_image_paths,
    )
    return dataset


def image_label_dataset_factory(
    image_paths, label_array, batch_size=64, image_size=(800, 800), repeat_times=None
):
    """
    dataset factory, returns TF dataset, which contains two tensor
    1. Image tensor
    2. Label tensor

    Parameters
    ----------
    image_paths: List[str]
        image_paths with length N
    label_array: np.ndarray
        label array, expected shape, N, C
    batch_size: int
        batch size
    image_size: int
        expected generated image shape
    repeat_times: Option[int]
        repeat the dataset or not, if None -> indefinitely

    Returns
    -------
    tf.data.Dataset
        result Dataset
    """
    decode_image_fn = partial(decode_image, image_size=image_size)

    dataset = (
        tf.data.Dataset.from_tensor_slices((image_paths, label_array))
        .map(decode_image_fn)
        .map(data_augment)
        .repeat(repeat_times)
        .shuffle(512)
        .batch(batch_size)
    )
    return dataset


def image_dataset_factory(image_paths, batch_size=64, image_size=(800, 800)):
    """
    dataset factory, returns TF dataset, which contains two tensor
    1. Image tensor

    Parameters
    ----------
    image_paths: List[str]
        image_paths with length N
    batch_size: int
        batch size
    image_size: int
        expected generated image shape

    Returns
    -------
    tf.data.Dataset
        result Dataset
    """
    decode_image_fn = partial(decode_image, image_size=image_size)

    dataset = (
        tf.data.Dataset.from_tensor_slices(image_paths)
        .map(decode_image_fn)
        .batch(batch_size)
    )
    return dataset
