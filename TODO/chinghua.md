# proposed solution

## get a baseline score from notebooks
- https://www.kaggle.com/anyexiezouqu/tpu-incepresnetv2-enb7?scriptVersionId=31040965

single model performance:
1. incetption_resnet_v2_20200404_vfold_5566_5_tpu: 0.962
2. incetption_resnet_v2_20200404_vfold_5566_5: --
3. efficient_net_b7_20200404_vfold_5566_5: 0.965
4. densenet121: --
models can be found here: https://drive.google.com/drive/folders/1V0_wepCDXvyWYEUxuuvn9V3EnTbJS92z?usp=sharing

### Ensemble scores

1 + 2 + 3 x fmin_ensemble.csv: 0.968
1 + 2 + 3 x naive_light_gbm.csv: 0.970
1 + 2 + 3 + 4 x naive_light_gbm.csv: 0.970


## apply pseudo labeling tricks
- adding testing set data into training set
- adjust training set data labels [url](https://www.kaggle.com/c/plant-pathology-2020-fgvc7/discussion/135051)

## TTA (testing argumentation)
- notebooks [tta](https://www.kaggle.com/miklgr500/plant-pathology-very-concise-tpu-efficient-tta-cu)
