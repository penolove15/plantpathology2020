# PlantPathology2020

## repo image

since current colab with tf 2.2.0rc, thus we select the tf image as the default image

`tensorflow/tensorflow:2.2.0rc2-gpu-py3-jupyter`


using docker-compose

```bash
docker-compose up
docker-compose exec tf bash

PYTHONPATH=src/ python src/util/flake_main.py 
```

## download the data with kaggle api for the first time startup

```bash
# Create API Token at your kaggle account https://www.kaggle.com/lianglu/account
# and set it as env variable
export KAGGLE_USERNAME=<your kaggle account>
export KAGGLE_KEY=<your kaggle key>
kaggle competitions download -c plant-pathology-2020-fgvc7
unzip plant-pathology-2020-fgvc7.zip -d data/
```

## available backbones

- inception_resnet
- efficient_net_b7
- densenet121


## training script

```bash
# pass the data_dir e.g. /usr/local/plantpathology2020/data
PYTHONPATH=src/ python scripts/training_script.py --data_dir /usr/local/plantpathology2020/data

# pass the data_dir e.g. /usr/local/plantpathology2020/data
PYTHONPATH=src/ python scripts/training_script.py --data_dir /usr/local/plantpathology2020/data --train_exclude_folds 5 --fold_pkl folds/5566_6.pkl --validation_folds 5 --model_dir inception_resnet_20200404_vfold_5566-5 --backbone_cfg_file backbone_cfg/inception_resnet.json

```

## testing script

```bash
PYTHONPATH=src/ python scripts/testing_script.py --data_dir /usr/local/plantpathology2020/data --fold_pkl folds/5566_6.pkl --validation_folds 5 --model_path inception_resnet_20200404_vfold_5566-5/best_model.h5 --backbone_cfg_file backbone_cfg/inception_resnet.json --test_output_path submission.csv --validation_output_path validation_out.pkl
```

## ensemble script

```bash
# directly average all the predictions
PYTHONPATH=src python scripts/ensemble/avg_models.py

# using fmin to find optimal ensemble weight
PYTHONPATH=src python scripts/ensemble/fmin_ensemble.py

# using lgb to train on validation set again
PYTHONPATH=src python scripts/ensemble/lgb_ensemble.py
```


## generate cv folds
```bash
# pass the data_dir e.g. /usr/local/plantpathology2020/data
PYTHONPATH=src/ python scripts/generate_cv_folds.py --data_dir /usr/local/plantpathology2020/data --output_path folds/5566_6.pkl --seeds 5566 --n_folds 6 
```


## train with the colab 

```bash
!git clone https://gitlab.com/penolove15/plantpathology2020.git
!PYTHONPATH=plantpathology2020/src/ python plantpathology2020/src/util/flake_main.py 
```

also, here is a naive training script (remember to fill in your account information)
https://colab.research.google.com/drive/1-tCNbc7OVtf4n7zrKVPVA5kMhgfkqIPB


## train with tpu in kaggle notebook

since tpu can only read the files from gs (google cloud storage) 
you need to start a kaggle kernel and execute following command to get the GCS information
```python
from kaggle_datasets import KaggleDatasets
GCS_DS_PATH = KaggleDatasets().get_gcs_path()
print(GCS_DS_PATH)
```

notice that the `KaggleDatasets().get_gcs_path()` only accessible in the kaggle kernel

more detail you can upload the notebook in the scripts/kaggle/kernel to kaggle platform


## [dev] make sure each commit can pass the linter

```bash
flake8
```

## [dev] repo ci

for each pr, we will only do the style check (please see .gitlab-ci.yml)
