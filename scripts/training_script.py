import os
import argparse
import json
from functools import partial
from pathlib import Path

import pandas as pd
import tensorflow as tf

from keras_utils.dataset import (
    image_label_dataset_factory,
    image_label_pesudo_labeling_dataset_factory,
    PseudoLabelingDataset,
)
from keras_utils.lr_scheduler import get_lr_scheduler
from keras_utils.models import model_factory, add_kernel_regularizer
from util.config import IMAGE_SIZE, DEFAULT_IMAGE_SIZE
from util.utils import get_folds_df


def prepare_validation_dataset(
    fold_pkl, validation_folds, train, image_size, batch_size, path_fn
):
    """
    Parameters
    ----------
    fold_pkl: str
        the fold pkl file
    validation_folds: List[int]
        the validation folds
    train: DataFrame
        the pandas dataframe used to slice the validation set

    Returns
    -------
    keras.Dataset
        the validation dataset
    """
    validation = get_folds_df(fold_pkl, validation_folds, train)
    validation_image_paths = validation.image_id.apply(path_fn).values
    validation_labels = validation.loc[:, "healthy":].values
    validation_dataset = image_label_dataset_factory(
        validation_image_paths,
        validation_labels,
        image_size=image_size,
        batch_size=batch_size,
        repeat_times=1,
    )
    return validation_dataset


def format_path(st, data_dir=""):
    return os.path.join(data_dir, "images", "%s.jpg" % st)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="the description for train user-embedding"
    )
    parser.add_argument("--data_dir", type=str, help="the data dir", required=True)
    parser.add_argument(
        "--backbone_cfg_file", type=str, help="backbone cfg", required=True
    )
    parser.add_argument(
        "--batch_size", type=int, default=2, help="batch size used for dataset"
    )
    parser.add_argument("--n_epochs", type=int, default=40, help="number of epoch")
    parser.add_argument("--tpu", dest="tpu", action="store_true", help="use tpu or not")
    parser.add_argument(
        "--train_exclude_folds", type=str, help="folds to be exclude", default=None
    )
    parser.add_argument("--fold_pkl", type=str, help="fold information", default=None)
    parser.add_argument(
        "--l2_regularizer", type=float, help="l2_regularizer", default=0
    )
    parser.add_argument(
        "--validation_folds", type=str, help="validation folds", default=None
    )
    parser.add_argument("--model_dir", type=str, help="validation folds", default="")
    parser.add_argument(
        "--pseudo_labling", dest="pseudo_labling", action="store_true", help="pseudo_labeling"
    )
    parser.set_defaults(replace=False)
    args = parser.parse_args()

    data_dir = args.data_dir
    batch_size = args.batch_size

    # make sure model_dir exists
    Path(args.model_dir).mkdir(parents=True, exist_ok=True)

    if not args.tpu:
        # expect you have a gpu device at gpu:0 and will be used for training
        strategy = tf.distribute.OneDeviceStrategy(device="/gpu:0")
        path_fn = partial(format_path, data_dir=data_dir)
    else:
        tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
        tf.config.experimental_connect_to_cluster(tpu)
        tf.tpu.experimental.initialize_tpu_system(tpu)
        strategy = tf.distribute.experimental.TPUStrategy(tpu)
        from kaggle_datasets import KaggleDatasets  # only available in kaggle

        gs_path = KaggleDatasets().get_gcs_path()
        path_fn = partial(format_path, data_dir=gs_path)
        batch_size *= strategy.num_replicas_in_sync

    n_epochs = args.n_epochs

    train_path = os.path.join(data_dir, "train.csv")
    train = pd.read_csv(train_path)

    with open(args.backbone_cfg_file) as f:
        backbone_cfg = json.load(f)

    if IMAGE_SIZE not in backbone_cfg:
        image_size = list(int(i) for i in DEFAULT_IMAGE_SIZE)
        backbone_cfg.update({IMAGE_SIZE: image_size})
    else:
        image_size = backbone_cfg[IMAGE_SIZE]

    # get validation dataset
    if args.validation_folds is not None:
        print("validation set from %s : %s" % (args.fold_pkl, args.validation_folds))
        validation_folds = [int(i) for i in args.validation_folds.split(",")]
        validation_dataset = prepare_validation_dataset(
            args.fold_pkl, validation_folds, train, image_size, batch_size, path_fn
        )
    else:
        validation_dataset = None

    if args.train_exclude_folds is not None:
        exclude_folds = [int(i) for i in args.train_exclude_folds.split(",")]
        train = get_folds_df(args.fold_pkl, exclude_folds, train, exclude=True)

    # get training dataset
    train_image_paths = train.image_id.apply(path_fn).values
    train_labels = train.loc[:, "healthy":].values
    enable_pseudo_labeling = True
    if enable_pseudo_labeling:
        test_path = os.path.join(data_dir, "test.csv")
        test = pd.read_csv(test_path)
        # get testing dataset
        test_image_paths = test.image_id.apply(path_fn).values
    else:
        test_image_paths = None

    if args.tpu:
        # tpu can't using generator, can only using tf dataset
        train_dataset = image_label_dataset_factory(
            train_image_paths,
            train_labels,
            image_size=image_size,
            batch_size=batch_size,
        )
    else:
        train_dataset = image_label_pesudo_labeling_dataset_factory(
            train_image_paths,
            train_labels,
            image_size=image_size,
            batch_size=batch_size,
            test_image_paths=test_image_paths,
        )

    # compile the model
    with strategy.scope():
        _, n_labels = train_labels.shape
        model = model_factory(backbone_cfg, n_labels)

        if args.l2_regularizer > 0:
            kernel_regularizer = tf.keras.regularizers.l2(args.l2_regularizer)
            add_kernel_regularizer(model, kernel_regularizer)

        model.compile(
            optimizer="adam",
            loss="categorical_crossentropy",
            metrics=["categorical_accuracy"],
        )
        model.summary()

    # n_epoch
    step_per_epoch = train_labels.shape[0] // batch_size

    # train the model
    lr_scheduler = get_lr_scheduler()
    if not validation_dataset:
        if isinstance(train_dataset, PseudoLabelingDataset):
            train_dataset = train_dataset.generate()
        history = model.fit(
            train_dataset,
            epochs=n_epochs,
            callbacks=[lr_scheduler],
            steps_per_epoch=step_per_epoch,
        )
        model.save(os.path.join(args.model_dir, "%s.h5" % n_epochs, overwrite=True))
    else:
        earlystoping = tf.keras.callbacks.EarlyStopping(
            monitor="val_categorical_accuracy",
            min_delta=1e-4,
            patience=3,
            verbose=1,
            mode="max",
        )

        model_check_point = tf.keras.callbacks.ModelCheckpoint(
            filepath=os.path.join(args.model_dir, "best_model.h5"),
            mode="max",
            save_best_only=True,
            monitor="val_categorical_accuracy",
            verbose=1,
        )
        callbacks = [lr_scheduler, earlystoping, model_check_point]

        if isinstance(train_dataset, PseudoLabelingDataset):
            train_dataset = train_dataset.generate()
            if args.pseudo_labling:
                callbacks.append(train_dataset)

        history = model.fit(
            train_dataset,
            epochs=n_epochs,
            callbacks=callbacks,
            steps_per_epoch=step_per_epoch,
            validation_data=validation_dataset,
            validation_freq=3,
        )
