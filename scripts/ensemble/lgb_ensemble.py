import numpy as np
import pandas as pd
import lightgbm as lgb
from sklearn.model_selection import StratifiedKFold

from util.ensemble import EnsembleCandidate
from util.utils import get_folds_df


def reverse_one_hot(X):
    label = np.zeros((X.shape[0],))
    for col in range(X.shape[1]):
        label[X[:, col] == 1] = col
    return label


if __name__ == "__main__":
    ensemble_paths = [
        "preds/densenet121_20200404_vfold_5566_5",
        "preds/inception_resnet_v2_20200404_vfold_5566_5",
        "preds/inception_resnet_v2_20200404_vfold_5566_5_tpu",
        "preds/efficient_net_b7_20200404_vfold_5",
        "preds/efficient_net_b4_20200406_vfold_5566_5",
        "preds/resnet50_20200405_vfold_5566_5"
    ]

    ensemble_candidates = [EnsembleCandidate(i) for i in ensemble_paths]
    X_validation = np.hstack([i.validation_pred for i in ensemble_candidates])

    folds_path = "folds/5566_6.pkl"
    train = pd.read_csv("data/train.csv")
    validation = get_folds_df(folds_path, [5], train)
    Y_validation = reverse_one_hot(validation.loc[:, "healthy":].values)
    skf = StratifiedKFold(n_splits=2, random_state=5566)

    # train ensemble model
    print(X_validation.shape, Y_validation.shape)
    train_valid_index = []
    for _, test_index in skf.split(X_validation, Y_validation):
        train_valid_index.append(test_index)

    ens_train_index = train_valid_index[0]
    ens_valid_index = train_valid_index[1]

    params = {
        "objective": "multiclass",
        "num_class": 4,
        "num_leaves": 2,
        "learning_rate": 5e-3,
        "feature_fraction": 0.25,
        "bagging_fraction": 1.0,
        "bagging_seed": 5566,
        "lambda_l2": 1e-4,
    }

    lg_train = lgb.Dataset(X_validation[ens_train_index, :], Y_validation[ens_train_index])
    lg_valid = lgb.Dataset(X_validation[ens_valid_index, :], Y_validation[ens_valid_index])
    lgb_model = lgb.train(
        params, lg_train, 8000, valid_sets=[lg_train, lg_valid], early_stopping_rounds=800
    )

    X_testing = np.hstack([i.testing_pred for i in ensemble_candidates])
    prob = lgb_model.predict(X_testing)

    sub = pd.read_csv("data/sample_submission.csv")
    sub.loc[:, "healthy":] = prob
    sub.to_csv("lgb_ensemble.csv", index=False)
