import pandas as pd
import numpy as np
from scipy.optimize import fmin
from sklearn.metrics import roc_auc_score
from functools import partial

from util.utils import get_folds_df
from util.ensemble import EnsembleCandidate


def eval_score(pred, Y):
    scores = []
    for i in range(pred.shape[1]):
        scores.append(roc_auc_score(Y[:, i], pred[:, i]))
    return -np.mean(scores)


def goals(v, Y, preds):
    result = [v_ * pred for v_, pred in zip(v, preds)]
    pred = sum(result)
    return eval_score(pred, Y)


if __name__ == "__main__":
    ensemble_paths = [
        "preds/densenet121_20200404_vfold_5566_5",
        "preds/inception_resnet_v2_20200404_vfold_5566_5",
        "preds/inception_resnet_v2_20200404_vfold_5566_5_tpu",
        "preds/efficient_net_b7_20200404_vfold_5",
    ]

    ensemble_candidates = [EnsembleCandidate(i) for i in ensemble_paths]
    X_validation = np.hstack([i.validation_pred for i in ensemble_candidates])

    train = pd.read_csv("data/train.csv")
    validation = get_folds_df("folds/5566_6.pkl", [5], train)
    Y = validation.loc[:, "healthy":].values

    validation_preds = [i.validation_pred for i in ensemble_candidates]

    opt = partial(goals, Y=Y, preds=validation_preds)

    coefs = fmin(opt, [0.0] * len(validation_preds))
    coefs = fmin(opt, coefs, ftol=0.000_001)
    coefs /= sum(coefs)
    print(coefs)

    testing_preds = [i.testing_pred for i in ensemble_candidates]
    predictions = [
        i * pred
        for i, pred in zip(coefs, testing_preds)
    ]

    sub = pd.read_csv("data/sample_submission.csv")
    sub.loc[:, "healthy":] = sum(predictions)
    sub.to_csv("fmin_ensemble.csv", index=False)
