import pandas as pd
from util.ensemble import EnsembleCandidate


if __name__ == "__main__":
    ensemble_paths = [
        "preds/densenet121_20200404_vfold_5566_5",
        "preds/inception_resnet_v2_20200404_vfold_5566_5",
        "preds/inception_resnet_v2_20200404_vfold_5566_5_tpu",
        "preds/efficient_net_b7_20200404_vfold_5",
        "preds/efficient_net_b4_20200406_vfold_5566_5",
        "preds/resnet50_20200405_vfold_5566_5"
    ]

    ensemble_candidates = [EnsembleCandidate(i) for i in ensemble_paths]
    testing_preds = [i.testing_pred for i in ensemble_candidates]
    predictions = sum(testing_preds) / len(testing_preds)

    sub = pd.read_csv("data/sample_submission.csv")
    sub.loc[:, "healthy":] = predictions
    sub.to_csv("avg_ensemble.csv", index=False)
