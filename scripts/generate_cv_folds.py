import os
import argparse
import pickle

import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="the description for train user-embedding"
    )
    parser.add_argument("--data_dir", type=str, help="the data dir")
    parser.add_argument("--output_path", type=str, help="output csv path")
    parser.add_argument("--seeds", type=int, help="seeds used to split")
    parser.add_argument("--n_folds", type=int, help="n_folds")
    args = parser.parse_args()
    data_dir = args.data_dir
    train_path = os.path.join(data_dir, "train.csv")
    train = pd.read_csv(train_path)
    y = train.loc[:, "healthy":].idxmax(axis=1)

    skf = StratifiedKFold(n_splits=args.n_folds, random_state=args.seeds)
    folds = np.zeros(len(y))
    for (fold_idx, (_, test_index)) in enumerate(skf.split(train, y)):
        folds[test_index] = fold_idx

    with open(args.output_path, 'wb') as f:
        pickle.dump(folds, file=f)
