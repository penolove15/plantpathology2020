import os
import pandas as pd
import argparse
import pickle
import json
from functools import partial

import tensorflow as tf
from keras_utils.dataset import image_dataset_factory
import efficientnet.tfkeras  # noqa: solve testing issue
from tensorflow.keras.models import load_model

from util.config import IMAGE_SIZE, DEFAULT_IMAGE_SIZE
from util.utils import get_folds_df


def format_path(st, data_dir=""):
    return os.path.join(data_dir, "images", "%s.jpg" % st)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="the description for train user-embedding"
    )
    parser.add_argument("--data_dir", type=str, help="the data dir", required=True)
    parser.add_argument(
        "--backbone_cfg_file", type=str, help="backbone cfg", required=True
    )
    parser.add_argument("--model_path", type=str, help="model path", required=True)

    parser.add_argument(
        "--test_output_path", type=str, help="testing output", required=True
    )

    parser.add_argument(
        "--batch_size", type=int, default=2, help="batch size used for dataset"
    )
    parser.add_argument("--tpu", dest="tpu", action="store_true", help="use tpu or not")

    parser.add_argument("--fold_pkl", type=str, help="fold information", default=None)
    parser.add_argument(
        "--validation_folds", type=str, help="validation folds", default=None
    )
    parser.add_argument(
        "--validation_output_path", type=str, help="validation output", default=""
    )

    parser.set_defaults(replace=False)

    args = parser.parse_args()
    data_dir = args.data_dir
    batch_size = args.batch_size

    if not args.tpu:
        # expect you have a gpu device at gpu:0 and will be used for training
        strategy = tf.distribute.OneDeviceStrategy(device="/gpu:0")
        path_fn = partial(format_path, data_dir=data_dir)
    else:
        tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
        tf.config.experimental_connect_to_cluster(tpu)
        tf.tpu.experimental.initialize_tpu_system(tpu)
        strategy = tf.distribute.experimental.TPUStrategy(tpu)
        from kaggle_datasets import KaggleDatasets  # only available in kaggle
        gs_path = KaggleDatasets().get_gcs_path()
        path_fn = partial(format_path, data_dir=gs_path)
        batch_size *= strategy.num_replicas_in_sync

    args = parser.parse_args()
    data_dir = args.data_dir
    test_path = os.path.join(data_dir, "test.csv")

    submission_path = os.path.join(data_dir, "sample_submission.csv")
    sub = pd.read_csv(submission_path)

    with open(args.backbone_cfg_file) as f:
        backbone_cfg = json.load(f)

    if IMAGE_SIZE not in backbone_cfg:
        image_size = list(int(i) for i in DEFAULT_IMAGE_SIZE)
    else:
        image_size = backbone_cfg[IMAGE_SIZE]

    test = pd.read_csv(test_path)
    # get testing dataset
    test_image_paths = test.image_id.apply(path_fn).values
    test_dataset = image_dataset_factory(
        test_image_paths, image_size=image_size, batch_size=batch_size
    )

    with open(args.backbone_cfg_file) as f:
        backbone_cfg = json.load(f)

    if IMAGE_SIZE not in backbone_cfg:
        image_size = list(int(i) for i in DEFAULT_IMAGE_SIZE)
        backbone_cfg.update({IMAGE_SIZE: image_size})
    else:
        image_size = backbone_cfg[IMAGE_SIZE]

    # load model
    model = load_model(args.model_path)

    # predict on test set
    prob = model.predict(test_dataset, verbose=1)
    sub.loc[:, "healthy":] = prob
    sub.to_csv(args.test_output_path, index=False)

    # get validation dataset
    if args.validation_folds is not None:
        train_path = os.path.join(data_dir, "train.csv")
        train = pd.read_csv(train_path)

        print("validation set from %s : %s" % (args.fold_pkl, args.validation_folds))
        validation_folds = [int(i) for i in args.validation_folds.split(",")]

        validation = get_folds_df(args.fold_pkl, validation_folds, train)
        validation_image_paths = validation.image_id.apply(path_fn).values

        validation_dataset = image_dataset_factory(
            validation_image_paths, image_size=image_size, batch_size=batch_size
        )

        # predict on validation set
        prob = model.predict(validation_dataset, verbose=1)
        with open(args.validation_output_path, 'wb') as f:
            pickle.dump(prob, file=f)
